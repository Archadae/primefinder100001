// Firsts.cpp : Defines the entry point for the console application.
//

#include "iostream"
#include "FirstGen.h"
#include "ctime"

int main()
{
	clock_t startTime = clock();
	PrimeGen fFinder;
	std::cout << fFinder.Find10001();
	std::cout << " in " << (clock() - startTime) << std::endl;
    return 0;
}

