#include "FirstGen.h"
#include "vector"
#include "math.h"
#include "algorithm"


const unsigned int NO_10001 = 104743;
const int FIRST_PRIMES[] = { 2, 3, 5, 7, 11, 13 };
std::vector <unsigned int> primesVect;

PrimeGen::PrimeGen() //konstruktor - populuje wektor wejsciowymi pierwszymi
{
	for (int i = 0; i < 6; i++)
	{
		primesVect.push_back(FIRST_PRIMES[i]);
	}
}

unsigned int PrimeGen::Find10001()							//szuka 10001szej liczby pierwszej
{
	int amountOfPrimes = 6;
	unsigned int currentNumber = 13;

	for (; amountOfPrimes < 10001; currentNumber += 2)		//wybiera kolejne liczby do sprawdzenia 
	{														//inkrementacja o 2 zeby nie sprawdzac parzystych
		bool isPrime = true;

		for (unsigned int vectorPos = 0; vectorPos < primesVect.size(); vectorPos++) //dzieli sprawdzana liczbe przez wszystkie pierwsze z wektora
		{
			if (currentNumber % primesVect[vectorPos] == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			primesVect.push_back(currentNumber);
			amountOfPrimes++;								//wszadza liczbe na wektor
		}
	}

	return primesVect[10000];
}

PrimeGen::~PrimeGen()
{
}
